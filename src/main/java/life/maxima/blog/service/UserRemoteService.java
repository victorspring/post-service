package life.maxima.blog.service;

import life.maxima.common.entity.cache.User;
import life.maxima.blog.repository.cache.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserRemoteService implements UserService {

    @Value("${user-service.endpoint}")
    private String usersEndpoint;

    private final RestOperations restOperations;
    private final UserRepository userRepository;

    @Override
    public String getUsername(String id) {
        Optional<User> user = userRepository.findById(id);

        String username;
        if (user.isPresent()){
            username = user.get().getUsername();
            log.info("Getting user from cache: " + username);
        } else {
            log.info("Calling user service");
            username = restOperations.getForObject(usersEndpoint + "/" + id , User.class)
                    .getUsername();
        }

        return username;

/*        return userRepository.findById(id).orElseGet(
                () -> restOperations.getForObject(usersEndpoint, User.class)).getName();*/
    }
}
